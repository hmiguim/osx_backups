#!/bin/bash

#
#   +-----------------------------------------------------------------------+
#   |                         Configuration Script                          |
#   +-----------------------------------------------------------------------+
#   |	Setups for:                                                         |
#   | 	- Dotfiles (.bash_profile & .inputrc)				    |
#   | 	- SSH Configuration 						    |
#   | 	- Git Configuration			                            |
#   | 	- Homebrew Configuration & Package installation                     |
#   | 	- Custom Scripts                                                    |
#   | 	- OSX Defaults Configuration                                        |
#   | 	- Gitshots Configuration                                            |
#   | 	- Manual Installation of imagesnap                                  |
#   +-----------------------------------------------------------------------+
#   | Authors: Miguel Guimar�es                                             |
#   +-----------------------------------------------------------------------+

set -e

source scripts/utils.sh

setup_gitconfig () {
  e_header 'Setup gitconfig'
  printf "\n"
  e_bold 'Enter your name and press [ENTER]:'
  read git_authorname
  e_bold 'Enter your email address and press [Enter]:'
  read git_authoremail

  git config --global user.name "$git_authorname"
  git config --global user.email "$git_authoremail"

  printf "\n"

  e_success 'gitconfig'
}

ssh_configuration() {
  ssh-keygen -t rsa -C "$email"
  ssh-add $HOME/.ssh/id_rsa
  cat $HOME/.ssh/id_rsa.pub | pbcopy

  e_success 'SSH configured'
}

setup_ssh() {
  e_header 'SSH Configuration'
  cd $HOME
  if [ ! -e $HOME/.ssh/id_rsa.pub ]; then
    ssh_configuration
  else
    seek_confirmation 'SSH already configured do you want to override it?'
    if is_confirmed; then
      e_arrow "Removing ssh folder"
      rm -rf $HOME/.ssh
      ssh_configuration
    else
      e_success "SSH configured"
    fi
  fi
}

install_dotfiles() {
  e_header 'Copying dotfiles'
  printf "\n"
  e_bold "Cleaning up"
  printf "\n"

  for f in `find dotfiles -type f`
  do
    e_bold "Copying: ".$f
    printf "\n"
    cp $f $HOME/.$(basename $f)
  done
}

install_scripts() {
  e_header 'Copying scripts'
  printf "\n"

  if [ -d $HOME/.scripts ]; then
    rm -rf $HOME/.scripts
    mkdir $HOME/.scripts
  else
    mkdir $HOME/.scripts
  fi

  for f in `find scripts -type f -name \*.sh`
  do
    e_bold "Copying: ".$f
    printf "\n"
    cp $f $HOME/.scripts/$(basename $f)
    e_bold "Permission: ".$f
    printf "\n"
    chmod +x $HOME/.scripts/$(basename $f)
  done

  e_success "Scripts copied"
}

install_preferences() {
  e_header 'Copying preferences'
  printf "\n"
  
  for f in `find prefs -type f`
  do
    cp $f ~/Library/Preferences/
  done

  e_success "Preferences installed"
}

install_brew() {
  e_header "Install brew"

  printf "\n"
  if type_exists 'brew'; then
    e_error $'If your intent is to reinstall you should do the following before running this installer again:\n   rm -rf /usr/local/Cellar /usr/local/.git && brew cleanup\n'
    e_success "Brew already installed"
    return 1
  fi

  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

  brew update
  # Upgrade any already-installed formulae
  brew upgrade

  # formulae installation
  # % -TAP- %
brew tap beeftornado/rmtree
brew tap caskroom/cask
brew tap gapple/services
brew tap homebrew/core
brew tap homebrew/science
brew tap watsonbox/cmu-sphinx
  # % -END- %
  # % -LIST- %
brew install ack
brew install adns
brew install ant
brew install archey
brew install autoconf
brew install automake
brew install bash
brew install bash-completion
brew install cmatrix
brew install cmu-pocketsphinx
brew install cmu-sphinxbase
brew install cmu-sphinxtrain
brew install cmuclmtk
brew install coreutils
brew install cowsay
brew install dirmngr
brew install ffmpeg
brew install findutils
brew install flac
brew install fontconfig
brew install fortune
brew install freetype
brew install gawk
brew install gcc
brew install gd
brew install gdbm
brew install gettext
brew install git
brew install gmp
brew install gnu-sed
brew install gnupg
brew install gnuplot
brew install gnutls
brew install gpg-agent
brew install grep
brew install icu4c
brew install imagemagick
brew install isl
brew install jpeg
brew install lame
brew install libassuan
brew install libffi
brew install libgcrypt
brew install libgeotiff
brew install libgpg-error
brew install libksba
brew install libmpc
brew install libogg
brew install libpng
brew install libsamplerate
brew install libsndfile
brew install libtasn1
brew install libtiff
brew install libtool
brew install libunistring
brew install libusb
brew install libusb-compat
brew install libvorbis
brew install libyaml
brew install little-cms2
brew install lua
brew install lzlib
brew install mad
brew install maven
brew install mongodb
brew install mpfr
brew install mysql
brew install neo4j
brew install neofetch
brew install nettle
brew install node
brew install npth
brew install numpy
brew install openjpeg
brew install openssl
brew install openssl@1.1
brew install p11-kit
brew install pcre
brew install pinentry
brew install pkg-config
brew install portaudio
brew install postgresql
brew install proj
brew install pth
brew install pv
brew install python
brew install python3
brew install readline
brew install rlwrap
brew install scipy
brew install screenresolution
brew install sl
brew install sox
brew install sqlite
brew install swig
brew install terminal-notifier
brew install tree
brew install wdiff
brew install webp
brew install wget
brew install x264
brew install xvid
brew install xz
brew install yarn
brew install youtube-dl
  # % -END- %

  e_bold "Cleaning up"
  printf "\n"

  brew cleanup

  e_success 'Brew installed'
}

setup_defaults_osx() {
  e_header 'Setup OSX defaults'
  printf "\n"
  source osx/set_defaults.sh
  killall Finder Dock

  e_success 'OSX defaults changed'
}

config_gitshots() {
  e_header "Configure git shots"
  printf "\n"
  e_arrow 'Copy the file in git/post-commit'
  e_arrow 'Find a git project'
  e_arrow 'Locate the .git/hooks folder'
  e_arrow 'Past the copy file into that folder'
  e_arrow 'Give permission to execute the post-commit file by using the chmod cmd'
  e_arrow 'Make a commit :)'

  printf "\n"

  e_success "Git shots configured"
}

config_printers() {
  printf "Access localhost:631\n"
  printf "Configure with this http: http://id5706:zSxc45s23d@printers.di.uminho.pt:443/printers/ricoh2060ps\n"
  printf "Generic\n"
  printf "PostScript\n"
  printf "Done\n"
}

#setup_gitconfig
#setup_ssh
#install_dotfiles
#install_scripts
#install_preferences
#install_brew
#setup_defaults_osx
#config_gitshots
#config_printers
