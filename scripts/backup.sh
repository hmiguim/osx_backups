#!/bin/bash

set -e

source ~/.scripts/utils.sh
source ~/.aliases
source ~/.functions

clear

BAK=$HOME/.backups

e_header "Cloning dotfiles repository"
git clone git@bitbucket.org:hmiguim/OSX_Backups.git $BAK

e_header "Starting to backup"

printf "\n\n"

e_arrow "Backing up the papers"

e_arrow "Checking the repository for new entries"

cd $HOME/Documents/papers

if [[ $(_parse_git_dirty) != "" ]]; then
  git add .
  git commit -m "** Backup script **"
  git push  
fi

e_success "Papers updated"

e_arrow "Backing up the PhD folder"

e_arrow "Checking the repository for new entries"

cd $HOME/Documents/PhD/thesis

if [[ $(_parse_git_dirty) != "" ]]; then
  git add .
  git commit -m "** Backup script **"
  git push  
fi

e_success "PhD folder updated"

cd $HOME

cp $HOME/.aliases $BAK/dotfiles/aliases
cp $HOME/.exports $BAK/dotfiles/exports
cp $HOME/.functions $BAK/dotfiles/functions
cp $HOME/.inputrc $BAK/dotfiles/inputrc
cp $HOME/.shortcuts $BAK/dotfiles/shortcuts
cp $HOME/.bash_profile $BAK/dotfiles/bash_profile
cp $HOME/.bash_prompt $BAK/dotfiles/bash_prompt
cp $HOME/.vimrc $BAK/dotfiles/vimrc

mkdir -p $BAK/prefs

cp ~/Library/Preferences/com.googlecode.iterm2.plist $BAK/prefs/com.googlecode.iterm2.plist

cp -r $HOME/.scripts/* $BAK/scripts

source $HOME/.scripts/brew_backup.sh $BAK

cd $BAK

if [[ $(_parse_git_dirty) != "" ]]; then
    git add .
    git commit -m "** Backup script **"
    git push
fi

cd $HOME

rm -rf $BAK

e_success "Finished"

terminal-notifier -message "All up-to-date" -title "OSX Backup" -subtitle "Finish" -sound default -remove ALL -sender com.apple.backup.launcher > /dev/null
