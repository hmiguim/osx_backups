#!/bin/bash

if [ -d .git ]; then
  git fetch -q

  BRANCH=$(git symbolic-ref HEAD 2>/dev/null | cut -d'/' -f3)

  if [ "$(git diff --shortstat 2> /dev/null | tail -n1)" != "" ] || [ $(git status --porcelain 2>/dev/null | grep "??" | wc -l) != "0" ]; then
    if [ $BRANCH != "master" ]; then
      git add --all :/
      read -ep "Message: " msg
      if [ -z "$msg" ]; then
        git commit -m "sem comentários"
      else
        git commit -m "$msg"
      fi
      git co master
      git merge $BRANCH
    else 
      git add --all :/
      read -ep "Message: " msg
      if [ -z "$msg" ]; then
        git commit -m "sem comentários"
      else
        git commit -m "$msg"
      fi
    fi
  fi

  UPSTREAM=${1:-"@{u}"}
  LOCAL=$(git rev-parse @)
  REMOTE=$(git rev-parse "$UPSTREAM")
  BASE=$(git merge-base @ "$UPSTREAM")

  if [ $LOCAL = $REMOTE ]; then
    echo "Up-to-date"
  elif [ $LOCAL = $BASE ]; then
    while true; do
      read -ep "Need to pull (Y/n)? " answer
      case $answer in
        [Yy]  ) git merge origin/master; break;;
        [Nn]  ) exit 1;;
        ""    ) git merge origin/master; break;;
        *     ) echo "Please answer yes or no.";;
      esac
    done
  elif [ $REMOTE = $BASE ]; then
    git push
  else
    echo "Diverged"
  fi
else
  echo "You must be in a git repository to use gup"
fi;
