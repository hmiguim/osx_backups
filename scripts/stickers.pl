#!/usr/bin/perl

use warnings;
use strict;
use utf8::all;
use DBI;
use List::MoreUtils qw(uniq);
use Scalar::Util qw(looks_like_number);
use POSIX qw(ceil);
use Switch;
use File::HomeDir;

my $dbname = "$HOME/.db/stickers.db";

my $db = DBI->connect("dbi:SQLite:uri=file:$dbname?mode=rwc");

my @new = ();

main();

sub main {
	my $cmd = "";

	print "\$- ";

	while(<>) {

		$cmd = $_;
		
		chomp $cmd;

		my @argv = split / /, $cmd;

		if (scalar @argv == 1) {
			switch ($cmd) {
				case ("exit") 		{ return 0; }
				case ("export") 	{ export(); }
				case ("update")		{ update(); }
				case ("progress")	{ progress(); }
				case ("clear")		{ clear(); }
				case ("check")		{ print "\nargs 404\n\n"; }
				case ("help")		{ print "\ncheck [args], replace, progress, export, exit\n\n"; }
				else 				{ print "\ncmd 404\n\n"; }
			}
		}

		if (scalar @argv > 1) {

			$cmd = shift @argv;

			if ($cmd eq 'check') {
				@argv = uniq @argv;
				check(@argv);
			} else {
				print "\ncmd 404\n\n";
			}
		}
		print "\$- ";

	}
}

sub update {
	if ((scalar @new) == 0) { return 0 };

	my $i = 0;

	for my $number (@new) {
		my $stmt = qq(update stickers set have=1 where number=$number;);
		my $rv = $db->do($stmt) or die $DBI::errstr;
		if($rv < 0) {
   			print $DBI::errstr;
		}
		$i++;
	}

	print "$i updated\n";

	@new = ();
}

sub check {
	my @numbers = @_;
	my $have = 0;

	my $n = 0;
	my $d = 0;

	for my $number (sort { $a <=> $b } @numbers) {

		if (looks_like_number($number) && $number > 0 && $number < 681) {

			my $stmt = qq(SELECT have from stickers where number=$number;);
			my $prepare = $db->prepare($stmt);
			my $result = $prepare->execute() or die $DBI::errstr;

			if($result < 0) {
				print $DBI::errstr;
   			}

   			while(my @row = $prepare->fetchrow_array()) {
      			$have = $row[0];
		    }

		    if (!$have) {
		    	push @new, $number;
				print $number, ":\t NEW\n";
				$n++;
		    } else {
		    	print $number, ":\t DUPLICATE\n";
		    	$d++;
		    }		
		}
	}
}

sub progress {
 		
 	my $stmt = qq(SELECT count(number) from stickers where have = 1);
	my $prepare = $db->prepare($stmt);
	my $result = $prepare->execute() or die $DBI::errstr;

	my $own = 0;
	my $total = 680;

	if($result < 0) {
		print $DBI::errstr;
	}

	while(my @row = $prepare->fetchrow_array()) {
		$own = $row[0];
    }

 	my $hashtags = ceil(($own * 40 ) / $total);
	my $spaces = 40 - $hashtags;
	my $perc = ($own / $total) * 100;

	chomp $own;

	print "\nStickers: [ ";
	print "#"x$hashtags;
	print " "x$spaces;
	print " ] " . ceil($perc) . "% - " . $own . "/$total (" . ($total-$own) . ")\n\n";
}

sub clear {
	@new = ();
}

sub export {
	my $stmt = qq(SELECT number from stickers where have = 0);
	my $prepare = $db->prepare($stmt);
	my $result = $prepare->execute() or die $DBI::errstr;

	my @cards = ();
	
	if($result < 0) {
		print $DBI::errstr;
	}

	while(my @row = $prepare->fetchrow_array()) {
		push @cards, $row[0];
	}

	my $path = File::HomeDir->my_desktop . "/export.csv";

    open my $fh, ">:utf8", $path or die "Can't create file: $!\n";

    my $string = "";
    my $i = 1;
    foreach my $card (sort { $a <=> $b } @cards) {
    		
    		$string .= $card;
    		if (($i % 10) == 0) {
    			$string .= "\n";
    		} else {
    			$string .= ",";
    		}
    		$i++;
		
   	}

   	print $fh $string;

    close $fh;
} 
