#!/bin/bash

#
# Set Colors
#

bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

purple=$(tput setaf 5)
red=$(tput setaf 1)
green=$(tput setaf 2)
tan=$(tput setaf 3)
blue=$(tput setaf 4)

#
# Headers and Logging
#

e_header() { 
  mantra $@
	printf "\n${tan}==========  %s  ==========${reset}\n" "$@" 
  mantra $@
  printf "\n"
}

e_arrow() {
	printf "➜ $@\n"
}

e_arrow_padding() {
  printf "${purple}\t➜ $@${reset}\n"
}

e_success() {
	printf "${green}✔ %s${reset}\n" "$@"
}

e_error() {
	printf "${red}✖ %s${reset}\n" "$@"
}

e_warning() { 
	printf "${tan}➜ %s${reset}\n" "$@"
}

e_underline() { 
	printf "${underline}${bold}%s${reset}\n" "$@"
}

e_bold() { 
	printf "${bold}%s${reset} " "$@"
}

e_note() { 
	printf "${underline}${bold}${blue}Note:${reset}  ${blue}%s${reset}\n" "$@"
}

seek_confirmation() {
  printf "\n${bold}$@${reset}"
  read -p " (y/n) " -n 1
  printf "\n"
}

# Test whether the result of an 'ask' is a confirmation
is_confirmed() {
	if [[ "$REPLY" =~ ^[Yy]$ ]]; then
	  return 0
	fi
	return 1
}

type_exists() {
	if [ $(type -P $1) ]; then
  		return 0
	fi
	return 1
}

mantra() {
  string=$@
  declare -i strlen
  strlen=${#string}
  
  strlen=$((strlen+24))
  
  printf "${tan}=%.s${reset}" $(eval "echo {1.."$(($strlen))"}");

}
