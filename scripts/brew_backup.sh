#!/bin/bash

set -e

TMP_DIR="/tmp"

brew tap > /dev/null

brew tap > $TMP_DIR/taps
brew list > $TMP_DIR/list

sed -i 's/^/brew tap /' $TMP_DIR/taps
sed -i 's/^/brew install /' $TMP_DIR/list

sed -i '/# % -TAP- %/,/# % -END- %/{//!d}' $1/install.sh
sed -i "/# % -TAP- %/r $TMP_DIR/taps" $1/install.sh

sed -i '/# % -LIST- %/,/# % -END- %/{//!d}' $1/install.sh 
sed -i "/# % -LIST- %/r $TMP_DIR/list" $1/install.sh

